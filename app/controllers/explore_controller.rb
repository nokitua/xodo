class ExploreController < ApplicationController
	inherit_resources
	has_scope :recent, :expiring, :successful, :in_funding, :recommended, :not_expired, type: :boolean
  def index
    @title = t('explore.title')

    @recommends = ProjectsForHome.recommends.includes(:project_total)
    @successful = Project.successful.limit(6).includes(:project_total)
    @failed = Project.failed.limit(6).includes(:project_total)
  end
end
